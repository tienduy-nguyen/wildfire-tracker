# Wildfire Tracker

Using React + Google Map React to track the wild on the world by [NASA data](https://api.nasa.gov)

## Getting started
- Init project
  I use [create-react-app](https://github.com/facebook/create-react-app) and Typescript to init a React Project
  ```bash
  $ npx create-react-app wildfire-tracker --template typescript
  # or
  $ yarn create react-app wildfire-tracker --template typescript 
  ```
- Install more important dependencies
  If you want to create  a complex app with **Router** and **Management State**, you need to install more dependencies after using **create-react-app**

  ```bash
  $ yarn add redux react-redux react-router react-router-dom redux-thunk
  $ yarn add @types/redux @types/react-redux @types/react-router @types/react-router-dom @types/redux-thunk
  ```
- Install google-map-react
  [google-map-react](https://github.com/google-map-react/google-map-react) ss a component written over a small set of the [Google Maps API](https://cloud.google.com/maps-platform/).It allows you to render any React component on the Google Map.

  ```bash
  $ yarn add google-map-react
  ```
- Using `dotenv` in create-react-app
  
  **react-scripts** actually uses dotenv library under the hood.
  With react-scripts@0.2.3 and higher, you can work with environment variables this way:

  - create .env file in the root of the project
  - set environment variables starting with REACT_APP_ there
  - access it by process.env.REACT_APP_... in components
  `.env` file
  ```
  REACT_APP_BASE_URL=http://localhost:3000
  ```

  `App.js`
  ```
  const BASE_URL = process.env.REACT_APP_BASE_URL;
  ```

  Check [Adding Custom Environment Variables](https://create-react-app.dev/docs/adding-custom-environment-variables/) for more details and check also the response on [stackoverflow](https://stackoverflow.com/questions/42182577/is-it-possible-to-use-dotenv-in-a-react-project#:~:text=The%20short%20answer%20is%20no,usually%20in%20a%20settings%20module.)

## Add Your React Components